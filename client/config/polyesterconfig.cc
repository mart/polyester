//////////////////////////////////////////////////////////////////////////////
// polyesterconfig.cc
// -------------------
// Config module for Polyester window decoration
// -------------------
// Copyright (c) 2005 Marco Martin
// -------------------
// derived from Smooth Blend
// Copyright (c) 2005 Ryan Nickell
// Please see the header file for copyright and license information.
//////////////////////////////////////////////////////////////////////////////

#include <kconfig.h>
#include <klocale.h>
#include <kglobal.h>
#include <qbuttongroup.h>
#include <qgroupbox.h>
#include <qradiobutton.h>
#include <qcheckbox.h>
#include <qspinbox.h>
#include <qwhatsthis.h>
#include <qcombobox.h>

#include "polyesterconfig.h"
#include "ui_configdialog.h"

//////////////////////////////////////////////////////////////////////////////
// polyesterConfig()
// -------------
// Constructor

polyesterConfig::polyesterConfig(KConfig* config, QWidget* parent)
        : QObject(parent), config_(0), dialog_(0) {
    // create the configuration object
    config_ = new KConfig("kwinpolyesterrc");
    KGlobal::locale()->insertCatalog("kwin_polyester_config");

    // create and show the configuration dialog
    dialog_ = new Ui::ConfigDialog();
    QWidget *configWidget = new QWidget(parent);
    dialog_->setupUi(configWidget);

    configWidget->show();
    // load the configuration
    load(config_);

    // setup the connections for title align
    connect(dialog_->titlealign, SIGNAL(clicked(int)),this, SLOT(selectionChanged(int)));
    // setup the connections for corner rounding
    connect(dialog_->roundCorners, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->titleBarStyle, SIGNAL(activated(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->buttonStyle, SIGNAL(activated(int)),this,SLOT(selectionChanged(int)));
    // setup title shadow
    connect(dialog_->titleshadow, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    // setup button actions
    connect(dialog_->animatebuttons, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->nomodalbuttons, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->btnComboBox, SIGNAL(activated(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->lightBorder, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    // setup the connections for spin boxes
    connect(dialog_->titlesize, SIGNAL(valueChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->buttonsize, SIGNAL(valueChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->framesize, SIGNAL(valueChanged(int)),this,SLOT(selectionChanged(int)));
    connect(dialog_->squareButton, SIGNAL(stateChanged(int)),this,SLOT(selectionChanged(int)));
    // double click the menu 
    connect(dialog_->menuClose, SIGNAL(stateChanged(int)),this, SLOT(selectionChanged(int)));
}

//////////////////////////////////////////////////////////////////////////////
// ~polyesterConfig()
// --------------
// Destructor

polyesterConfig::~polyesterConfig() {
    if (dialog_)
    {
        delete dialog_;
    }
    if (config_)
    {
        delete config_;
    }
}

//////////////////////////////////////////////////////////////////////////////
// selectionChanged()
// ------------------
// Selection has changed

void polyesterConfig::selectionChanged(int) {

    if(dialog_->buttonsize->value() + dialog_->framesize->value() > dialog_->titlesize->value())
    {
        dialog_->buttonsize->setValue(dialog_->titlesize->value()- dialog_->framesize->value());
    }
    // setting the framesize to less than 2 will lose the top gradient and look flat
   /* if(dialog_->framesize->value() < 2)
    {
        dialog_->framesize->setValue(2);
    }*/
    emit changed();
}

//////////////////////////////////////////////////////////////////////////////
// load()
// ------
// Load configuration data

void polyesterConfig::load(KConfig*) {
    KConfigGroup cg(config_, "General");

    QString value = cg.readEntry("TitleAlignment", "AlignHCenter");
    QRadioButton *button = dialog_->titlealign->findChild<QRadioButton*>(value);
    if (button)
    {
        button->setChecked(true);
    }

    dialog_->titlesize->setValue( cg.readEntry("TitleSize",20 ) );
    dialog_->buttonsize->setValue( cg.readEntry("ButtonSize",18 ) );
    dialog_->squareButton->setChecked( cg.readEntry("SquareButton", false ) );
    dialog_->framesize->setValue( cg.readEntry("FrameSize",2 ) );

    bool cornersFlag = cg.readEntry("RoundCorners", true);
    dialog_->roundCorners->setChecked( cornersFlag );
    bool titleshadow = cg.readEntry("TitleShadow", true);
    dialog_->titleshadow->setChecked(titleshadow);
    bool animatebuttons = cg.readEntry("AnimateButtons", true);
    dialog_->animatebuttons->setChecked(animatebuttons);
    bool lightBorder = cg.readEntry("LightBorder", true);
    dialog_->lightBorder->setChecked(lightBorder);
    dialog_->titleBarStyle->setCurrentItem(cg.readEntry("TitleBarStyle",0));
    dialog_->buttonStyle->setCurrentItem(cg.readEntry("ButtonStyle",1));
    bool nomodalbuttons = cg.readEntry("NoModalButtons", false);
    dialog_->nomodalbuttons->setChecked(nomodalbuttons);
    dialog_->btnComboBox->setCurrentItem(cg.readEntry("ButtonComboBox",0));
    bool menuClose = cg.readEntry("CloseOnMenuDoubleClick", true);
    dialog_->menuClose->setChecked(menuClose);
}

//////////////////////////////////////////////////////////////////////////////
// save()
// ------
// Save configuration data

void polyesterConfig::save(KConfig*) {
    KConfigGroup cg(config_, "General");

    QRadioButton *button = (QRadioButton*)dialog_->titlealign->selected();
    if (button)
    {
        cg.writeEntry("TitleAlignment", QString(button->name()));
    }
    cg.writeEntry("RoundCorners", dialog_->roundCorners->isChecked() );
    cg.writeEntry("TitleSize", dialog_->titlesize->value() );
    cg.writeEntry("ButtonSize", dialog_->buttonsize->value() );
    cg.writeEntry("SquareButton", dialog_->squareButton->isChecked() );
    cg.writeEntry("FrameSize", dialog_->framesize->value() );
    cg.writeEntry("TitleShadow", dialog_->titleshadow->isChecked() );
    cg.writeEntry("TitleBarStyle", dialog_->titleBarStyle->currentItem());
    cg.writeEntry("ButtonStyle", dialog_->buttonStyle->currentItem());
    cg.writeEntry("AnimateButtons", dialog_->animatebuttons->isChecked() );
    cg.writeEntry("LightBorder", dialog_->lightBorder->isChecked() );
    cg.writeEntry("NoModalButtons", dialog_->nomodalbuttons->isChecked() );
    cg.writeEntry("ButtonComboBox", dialog_->btnComboBox->currentItem());
    cg.writeEntry("CloseOnMenuDoubleClick", dialog_->menuClose->isChecked() );

    config_->sync();
}

//////////////////////////////////////////////////////////////////////////////
// defaults()
// ----------
// Set configuration defaults

void polyesterConfig::defaults() {
    QRadioButton *button = (QRadioButton*)dialog_->titlealign->child("AlignHCenter");
    if (button)
    {
        button->setChecked(true);
    }
    dialog_->roundCorners->setChecked( true );
    dialog_->titlesize->setValue( 20 );
    dialog_->squareButton->setChecked( false );
    dialog_->buttonsize->setValue( 18 );
    dialog_->framesize->setValue( 2 );
    dialog_->titleBarStyle->setCurrentItem( 0 );
    dialog_->buttonStyle->setCurrentItem( 1 );
    dialog_->titleshadow->setChecked( true );
    dialog_->animatebuttons->setChecked( true );
    dialog_->nomodalbuttons->setChecked( true );
    dialog_->lightBorder->setChecked( true );
    dialog_->btnComboBox->setCurrentItem( 0 );
    dialog_->menuClose->setChecked( false );
}

//////////////////////////////////////////////////////////////////////////////
// Plugin Stuff                                                             //
//////////////////////////////////////////////////////////////////////////////

extern "C" {
    QObject* allocate_config(KConfig* config, QWidget* parent) {
        return (new polyesterConfig(config, parent));
    }
}

#include "polyesterconfig.moc"
