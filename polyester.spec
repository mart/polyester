%define version	1.9.0
%define name	polyester
%define prefix	%(kde4-config --prefix)

Name: %{name}
Version: %{version}
Release: 1
Vendor: notmart@gmail.com
Url: http://kdelook.org/content/show.php?content=27968
License: LGPL
Summary: A glossy kde theme
Group: System/GUI/KDE
Packager: Marco Martin <notmart@gmail.com>
BuildRoot:  %{_tmppath}/%{name}-root 
Source: %{name}-%{version}.tar.gz

%description
Tasty Menu is a KMenu replacement aiming to provide the maximum usability, or at least to be a testbed for usability concepts and ideas for a future KDE menu.

%prep
%setup


%build
cmake . -DCMAKE_INSTALL_PREFIX=%{prefix} -DCMAKE_BUILD_TYPE=release -DDATA_INSTALL_DIR=%{prefix}/share/kde4/apps -DKCFG_INSTALL_DIR=%{prefix}/share/kde4/config.kcfg -DTEMPLATES_INSTALL_DIR=%{prefix}/share/kde4/templates
make

%install
make install DESTDIR=%{buildroot}
cd %{buildroot}
find . -type d | sed '1,2d;s,^\.,\%attr(-\,root\,root) \%dir ,' > /var/tmp/file.list.%{name}
find . -type f | sed 's,^\.,\%attr(-\,root\,root) ,' >>  /var/tmp/file.list.%{name}
find . -type l | sed 's,^\.,\%attr(-\,root\,root) ,' >>  /var/tmp/file.list.%{name}


%clean
rm -rf %{buildroot}
rm -rf /var/tmp/file.list.%{name}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig
%files -f /var/tmp/file.list.%{name}
%defattr(0644, root, root)


%changelog
*Wed Feb 6 2008 notmart@gmail.com
1.9.0 (2.0 beta1):
-initial release

