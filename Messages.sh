#! /usr/bin/env bash
xgettext --foreign-user  --from-code=UTF-8 -C -ktr2i18n   ./style/config/ui_configdialog.h -o ./po/kstyle_polyester_config.pot

for lang in it pl
do
  msgmerge  po/$lang/kstyle_polyester_config.po po/kstyle_polyester_config.pot -o po/$lang/kstyle_polyester_config.po
done