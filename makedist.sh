#!/bin/sh

mkdir build
cd build
make package_source
cp *.tar.bz2 /usr/src/packages/SOURCES
rpmbuild -bb ../polyester.spec
mv /usr/src/packages/RPMS/i586/*.rpm .
rm /usr/src/packages/SOURCES/*.tar.bz2
