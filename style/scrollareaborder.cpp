/* Polyester widget style for KDE 4
   Copyright (C) 2006 Marco Martin <notmart@gmail.com>

   based on Plastik widget style for KDE 3
   Copyright (C) 2003 Sandro Giessl <ceebx@users.sourceforge.net>

   based on the KDE style "dotNET":

   Copyright (C) 2001-2002, Chris Lee <clee@kde.org>
                            Carsten Pfeiffer <pfeiffer@kde.org>
                            Karol Szwed <gallium@kde.org>
   Drawing routines completely reimplemented from KDE3 HighColor, which was
   originally based on some stuff from the KDE2 HighColor.

   based on drawing routines of the style "Keramik":

   Copyright (c) 2002 Malte Starostik <malte@kde.org>
             (c) 2002,2003 Maksim Orlovich <mo002j@mail.rochester.edu>
   based on the KDE3 HighColor Style
   Copyright (C) 2001-2002 Karol Szwed      <gallium@kde.org>
             (C) 2001-2002 Fredrik Höglund  <fredrik@kde.org>
   Drawing routines adapted from the KDE2 HCStyle,
   Copyright (C) 2000 Daniel M. Duley       <mosfet@kde.org>
             (C) 2000 Dirk Mueller          <mueller@kde.org>
             (C) 2001 Martijn Klingens      <klingens@kde.org>
   Progressbar code based on KStyle,
   Copyright (C) 2001-2002 Karol Szwed <gallium@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
 */

#include "scrollareaborder.h"

#include <QPainter>
#include <QAbstractScrollArea>
#include <stdio.h>

ScrollAreaBorder::ScrollAreaBorder( Orientation orient,
                                    const QColor &col,
                                    const QColor &highlightCol,
                                    const int contrast,
                                    const bool drawShadow,
                                    QWidget *parent ) : QWidget(parent)
{
  o = orient;
  borderColor = col;
  highlightColor = highlightCol;
  //more the color is dark more is transparent
  alpha = contrast*5;
  shadow = drawShadow;
  highlightAlpha = highlightCol.value()/4 + contrast*5;
}

ScrollAreaBorder::~ScrollAreaBorder(){}


void ScrollAreaBorder::paintEvent(QPaintEvent *)
{
  QPainter painter(this);
  QLinearGradient gradient;
  QRect r(rect());
  QRect portRect;

  QRegion mask(r);

  bool hasFocus = static_cast<QWidget*>(parent())->hasFocus();

  int frameWidth = 0, frameHeight = 0, portWidth = 0, portHeight = 0;
  bool drawShadow = shadow;
  if( QAbstractScrollArea *area = qobject_cast<QAbstractScrollArea*>(parentWidget()) )
  {
    if( area->testAttribute(Qt::WA_NoSystemBackground) ) {
        return;
    }

    frameWidth = area->frameRect().width();
    frameHeight = area->frameRect().height();
    portWidth =area->viewport()->rect().width()+2;
    portHeight = area->viewport()->rect().height()+2;
    portRect = area->viewport()->rect();
    portRect = QRect(area->viewport()->pos().x()-1,
                     area->viewport()->pos().y()-1,
                     portRect.width(), portRect.height());
  }

  switch(o)
  {
    case ScrollAreaBorder::Left:
    {
      gradient = QLinearGradient(r.left(), r.center().y(), r.right(), r.center().y());
      int diff = frameHeight - portHeight;
      if( portRect.x()>0 )
        drawShadow = false;
      else if( diff>0 )
        r = QRect(portRect.x(), portRect.y(), r.width(), portRect.height());

      mask -= QRegion(r.left(), r.top(), 1, 1);
      mask -= QRegion(r.left(), r.bottom(), 1, 1);

      break;
    }
    case ScrollAreaBorder::Right:
    {
      gradient = QLinearGradient(r.right(), r.center().y(), r.left(), r.center().y());
      int diff = frameHeight - portHeight;
      if( diff>0 )
        r = QRect(r.x(), portRect.y(), r.width(), portRect.height());

      mask -= QRegion(r.left(), r.top(), 1, 1);
      mask -= QRegion(r.left(), r.bottom(), 1, 1);

      break;
    }
    case ScrollAreaBorder::Top:
    {
      painter.setPen(hasFocus?highlightColor:borderColor);
      painter.drawPoint(r.x(), r.y());
      painter.drawPoint(r.right(), r.y());
      gradient = QLinearGradient(r.center().x(), r.top(), r.center().x(), r.bottom());
      int diff = frameWidth - portWidth;
      if( portRect.y() > 0 )
        drawShadow = false;
      else if( diff>0 )
        r = QRect(portRect.x(), portRect.y(), portRect.width(), r.height());

      if( portRect.x() == 0 )
        mask -= QRegion(r.left(), r.top(), 1, 1);
      if( portRect.right() == rect().right() )
        mask -= QRegion(r.right(), r.top(), 1, 1);

      break;
    }
    case ScrollAreaBorder::Bottom:
    {
      painter.setPen(hasFocus?highlightColor:borderColor);
      painter.drawPoint(r.x(), r.bottom());
      painter.drawPoint(r.right(), r.bottom());
      gradient = QLinearGradient(r.center().x(), r.bottom(), r.center().x(), r.top());
      int diff = frameWidth - portWidth;
      if( diff>0 )
        r = QRect(portRect.x(), portRect.y(), portRect.width(), r.height());

      if( portRect.x() == 0 )
        mask -= QRegion(r.left(), r.bottom(), 1, 1);
      if( portRect.right() == rect().right() )
        mask -= QRegion(r.right(), r.bottom(), 1, 1);

      break;
    }
  }
  if( drawShadow )
  {
    if( o == ScrollAreaBorder::Bottom )
    {
      gradient.setColorAt(0, QColor(255,255,255,128));
      gradient.setColorAt(1, QColor(255,255,255,0));
    }
    else if( hasFocus )
    {
      QColor shadowCol = highlightColor;
      shadowCol.setAlpha(highlightAlpha);
      gradient.setColorAt(0, shadowCol);
      shadowCol.setAlpha(0);
      gradient.setColorAt(1, shadowCol);
    }
    else
    {
      gradient.setColorAt(0, QColor(0,0,0,alpha));
      gradient.setColorAt(1, QColor(0,0,0,0));
    }
    painter.setClipRegion(mask);
    painter.fillRect(r, gradient);
  }

}


