HEADERS = misc.h polyester.h scrollareaborder.h
SOURCES = polyester.cpp drawcontrol.cpp scrollareaborder.cpp misc.cpp

TEMPLATE = lib
PLUGIN = true
CONFIG += qt x11 plugin
QT += qt3support
VERSION       = 1.90
target.path += $$[QT_INSTALL_PLUGINS]/styles
DATA.files = polyester.themerc
DATA.path += $$(KDEDIR)/share/apps/kstyle/themes/
INSTALLS += target DATA
DEFINES += NO_KDE