/* Polyester widget style for KDE 4
   Copyright (C) 2006 Marco Martin <notmart@gmail.com>

   based on Plastik widget style for KDE 3
   Copyright (C) 2003 Sandro Giessl <ceebx@users.sourceforge.net>

   based on the KDE style "dotNET":

   Copyright (C) 2001-2002, Chris Lee <clee@kde.org>
                            Carsten Pfeiffer <pfeiffer@kde.org>
                            Karol Szwed <gallium@kde.org>
   Drawing routines completely reimplemented from KDE3 HighColor, which was
   originally based on some stuff from the KDE2 HighColor.

   based on drawing routines of the style "Keramik":

   Copyright (c) 2002 Malte Starostik <malte@kde.org>
             (c) 2002,2003 Maksim Orlovich <mo002j@mail.rochester.edu>
   based on the KDE3 HighColor Style
   Copyright (C) 2001-2002 Karol Szwed      <gallium@kde.org>
             (C) 2001-2002 Fredrik Höglund  <fredrik@kde.org>
   Drawing routines adapted from the KDE2 HCStyle,
   Copyright (C) 2000 Daniel M. Duley       <mosfet@kde.org>
             (C) 2000 Dirk Mueller          <mueller@kde.org>
             (C) 2001 Martijn Klingens      <klingens@kde.org>
   Progressbar code based on KStyle,
   Copyright (C) 2001-2002 Karol Szwed <gallium@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
 */

#ifndef __SCROLLAREABORDER_H
#define __SCROLLAREABORDER_H

#include <QObject>
#include <QPalette>
#include <QtGui/QWidget>

class ScrollAreaBorder : public QWidget
{
  Q_OBJECT

  public:
    enum Orientation
    {
      Top,
      Bottom,
      Left,
      Right
    };

  private:
    Orientation o;
    QColor borderColor;
    QColor highlightColor;
    int alpha;
    int highlightAlpha;
    bool shadow;

  public:
    ScrollAreaBorder( Orientation orient,
                      const QColor &col,
                      const QColor &highlightCol,
                      const int contrast,
                      const bool drawShadow,
                      QWidget *parent = 0 );
    ~ScrollAreaBorder();

    Orientation orientation(){return o;}

  protected:
    void paintEvent(QPaintEvent *);


};

#endif // __SCROLLAREABORDER_H
