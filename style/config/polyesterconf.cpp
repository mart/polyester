/*
Polyester widget style for KDE 4
Copyright (C) 2006 Marco Martin <notmart@gmail.com>
based on the Keramik and Plastik configuration dialog:

Plastik:
Copyright (C) 2003 Sandro Giessl <ceebx@users.sourceforge.net>

Keramik:
Copyright (c) 2003 Maksim Orlovich <maksim.orlovich@kdemail.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QLayout>
//#include <QHBox>
#include <QSpinBox>
#include <QSettings>
#include <QColor>
#include <QGroupBox>
#include <KGlobal>
#include <KLocale>
#include <KColorButton>
//#include <KDEMacros>

#include "polyesterconf.h"

extern "C"
{
	KDE_EXPORT QWidget* allocate_kstyle_config(QWidget* parent)
	{
		KGlobal::locale()->insertCatalog("kstyle_polyester_config");
		return new PolyesterStyleConfig(parent);
	}
}

PolyesterStyleConfig::PolyesterStyleConfig(QWidget* parent): QWidget(parent)
{


	dialog_ = new Ui_ConfigDialog();
        dialog_->setupUi(this);
	KGlobal::locale()->insertCatalog("kstyle_polyester_config");




        QSettings s("Polyester", "Style");
        s.beginGroup("Style");
	origScrollBarLines = s.value("scrollBarLines", false).toBool();
	dialog_->scrollBarLines->setChecked(origScrollBarLines);
	origColoredScrollBar = s.value("coloredScrollBar", true).toBool();
	dialog_->coloredScrollBar->setChecked(origColoredScrollBar);
	origScrollBarStyle = s.value("scrollBarStyle", "ThreeButtonScrollBar").toString();
	dialog_->scrollBarStyle->setCurrentIndex(scrollBarItem(origScrollBarStyle));
	origScrollBarSize = s.value("scrollBarSize", 16).toInt();
	dialog_->scrollBarSize->setValue(origScrollBarSize);
	origCenteredTabBar = s.value("centeredTabBar", false).toBool();
	dialog_->centeredTabBar->setChecked(origCenteredTabBar);
	origHighLightTab = s.value("highLightTab", true).toBool();
	dialog_->highLightTab->setChecked(origHighLightTab);
	origStatusBarFrame = s.value("statusBarFrame", true).toBool();
	dialog_->statusBarFrame->setChecked(origStatusBarFrame);

        //MENUS
	origMenuItemSpacing = s.value("menuItemSpacing", 8).toInt();
	dialog_->menuItemSpacing->setValue(origMenuItemSpacing);
	origButtonMenuItem = s.value("buttonMenuItem", true).toBool();
	dialog_->buttonMenuItem->setChecked(origButtonMenuItem);
	origMenuBarEmphasis = s.value("menuBarEmphasis", false).toBool();
	dialog_->menuBarEmphasis->setChecked(origMenuBarEmphasis);
	origMenuBarEmphasisBorder = s.value("menuBarEmphasisBorder", true).toBool();
	dialog_->menuBarEmphasisBorder->setChecked(origMenuBarEmphasisBorder);
	origCustomMenuBarEmphasisColor = s.value("customMenuBarEmphasisColor", false).toBool();
	dialog_->customMenuBarEmphasisColor->setChecked(origCustomMenuBarEmphasisColor);

	origMenuBarEmphasisColor = s.value("menuBarEmphasisColor", "black").toString();
	dialog_->menuBarEmphasisColor->setColor(origMenuBarEmphasisColor);

	origMenuStripe = s.value("menuStripe", true).toBool();
	dialog_->menuStripe->setChecked(origMenuStripe);

	origShadowedButtonsText = s.value("shadowedButtonsText", true).toBool();
	dialog_->shadowedButtonsText->setChecked(origShadowedButtonsText);
        origShadowedMenuBarText = s.value("shadowedMenuBarText", true).toBool();
        dialog_->shadowedMenuBarText->setChecked(origShadowedMenuBarText);
        origSunkenShadows = s.value("sunkenShadows", true).toBool();
        dialog_->sunkenShadows->setChecked(origSunkenShadows);

        //MISC
        origColorizeSortedHeader = s.value("colorizeSortedHeader", true).toBool();
        dialog_->colorizeSortedHeader->setChecked(origColorizeSortedHeader);
	origUseLowerCaseText = s.value("useLowerCaseText", false).toBool();
	dialog_->useLowerCaseText->setChecked(origUseLowerCaseText);
	origAnimProgressBar = s.value("animateProgressBar", false).toBool();
	dialog_->animateProgressBar->setChecked(origAnimProgressBar);
	origLightBorder = s.value("lightBorder", true).toBool();
	dialog_->lightBorder->setChecked(origLightBorder);
	origAnimButton = s.value("animateButton", false).toBool();
	dialog_->animateButton->setChecked(origAnimButton);
	origAnimButtonToDark = s.value("animateButtonToDark", false).toBool();
	dialog_->animateButtonToDark->setChecked(origAnimButtonToDark);
	origDrawToolBarSeparator = s.value("drawToolBarSeparator", true).toBool();
	dialog_->drawToolBarSeparator->setChecked(origDrawToolBarSeparator);
	origDrawToolBarItemSeparator = s.value("drawToolBarItemSeparator", true).toBool();
	dialog_->drawToolBarItemSeparator->setChecked(origDrawToolBarItemSeparator);
//	origDrawFocusRect = s.value("drawFocusRect", true).toBool();
//	drawFocusRect->setChecked(origDrawFocusRect);
	origDrawTriangularExpander = s.value("drawTriangularExpander", false).toBool();
	dialog_->drawTriangularExpander->setChecked(origDrawTriangularExpander);
	origInputFocusHighlight = s.value("inputFocusHighlight", true).toBool();
	dialog_->inputFocusHighlight->setChecked(origInputFocusHighlight);
	origButtonStyle = s.value("buttonStyle", "glass").toString();
	dialog_->buttonStyle->setCurrentIndex(buttonItem(origButtonStyle));
	origCustomOverHighlightColor = s.value("customOverHighlightColor", false).toBool();
	dialog_->customOverHighlightColor->setChecked(origCustomOverHighlightColor);
	origOverHighlightColor = s.value("overHighlightColor", "black").toString();
	dialog_->overHighlightColor->setColor(origOverHighlightColor);
	origCustomFocusHighlightColor = s.value("customFocusHighlightColor", false).toBool();
	dialog_->customFocusHighlightColor->setChecked(origCustomFocusHighlightColor);
	origFocusHighlightColor = s.value("focusHighlightColor", "black").toString();
	dialog_->focusHighlightColor->setColor(origFocusHighlightColor);
	origCustomCheckMarkColor = s.value("customCheckMarkColor", false).toBool();
	dialog_->customCheckMarkColor->setChecked(origCustomCheckMarkColor);
	origCheckMarkColor = s.value("checkMarkColor", "black").toString();
	dialog_->checkMarkColor->setColor(origCheckMarkColor);

	connect(dialog_->useLowerCaseText, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->scrollBarLines, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->scrollBarStyle, SIGNAL( activated(int) ), SLOT( updateChanged() ) );
	connect(dialog_->scrollBarSize, SIGNAL( valueChanged( int ) ), SLOT( updateChanged() ) );
	connect(dialog_->coloredScrollBar, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->centeredTabBar, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->highLightTab, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->statusBarFrame, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->menuItemSpacing, SIGNAL( valueChanged(int) ), SLOT( updateChanged() ) );
	connect(dialog_->buttonMenuItem, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->menuBarEmphasis, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->menuBarEmphasisBorder, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->customMenuBarEmphasisColor, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->menuBarEmphasisColor, SIGNAL( changed(const QColor&) ), SLOT( updateChanged() ) );
	connect(dialog_->menuStripe, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->shadowedButtonsText, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
        connect(dialog_->shadowedMenuBarText, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
        connect(dialog_->sunkenShadows, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->animateProgressBar, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->lightBorder, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->animateButton, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->animateButtonToDark, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->drawToolBarSeparator, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->drawToolBarItemSeparator, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->drawTriangularExpander, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->inputFocusHighlight, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
        connect(dialog_->colorizeSortedHeader, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->buttonStyle, SIGNAL( activated(int) ), SLOT( updateChanged() ) );
	connect(dialog_->customOverHighlightColor, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->overHighlightColor, SIGNAL( changed(const QColor&) ), SLOT( updateChanged() ) );
	connect(dialog_->customFocusHighlightColor, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->focusHighlightColor, SIGNAL( changed(const QColor&) ), SLOT( updateChanged() ) );
	connect(dialog_->customCheckMarkColor, SIGNAL( toggled(bool) ), SLOT( updateChanged() ) );
	connect(dialog_->checkMarkColor, SIGNAL( changed(const QColor&) ), SLOT( updateChanged() ) );

	if ( dialog_->animateButton->isChecked() )
	 dialog_->animateButtonToDark->setEnabled(true);
	else
	 dialog_->animateButtonToDark->setEnabled(false);

        if ( dialog_->menuBarEmphasis->isChecked() )
        {
         dialog_->menuBarEmphasisBorder->setEnabled(true);
         dialog_->customMenuBarEmphasisColor->setEnabled(true);
         if ( dialog_->customMenuBarEmphasisColor->isChecked() )
           dialog_->menuBarEmphasisColor->setEnabled(true);
         else
           dialog_->menuBarEmphasisColor->setEnabled(false);
        }
        else
        {
         dialog_->menuBarEmphasisBorder->setEnabled(false);
         dialog_->customMenuBarEmphasisColor->setEnabled(false);
         dialog_->menuBarEmphasisColor->setEnabled(false);
        }

	if ( dialog_->customOverHighlightColor->isChecked() )
	 dialog_->overHighlightColor->setEnabled(true);
	else
	 dialog_->overHighlightColor->setEnabled(false);

	if ( dialog_->customFocusHighlightColor->isChecked() )
	 dialog_->focusHighlightColor->setEnabled(true);
	else
	 dialog_->focusHighlightColor->setEnabled(false);

	if ( dialog_->customCheckMarkColor->isChecked() )
	 dialog_->checkMarkColor->setEnabled(true);
	else
	 dialog_->checkMarkColor->setEnabled(false);
}

PolyesterStyleConfig::~PolyesterStyleConfig()
{
}


void PolyesterStyleConfig::save()
{
	QSettings s("Polyester", "Style");
        s.beginGroup("Style");
	s.setValue("useLowerCaseText", dialog_->useLowerCaseText->isChecked());
	s.setValue("scrollBarLines", dialog_->scrollBarLines->isChecked());
	s.setValue("scrollBarStyle",
                     scrollBarType(dialog_->scrollBarStyle->currentIndex()));
	s.setValue("scrollBarSize", dialog_->scrollBarSize->value());
	s.setValue("coloredScrollBar", dialog_->coloredScrollBar->isChecked());
	s.setValue("centeredTabBar", dialog_->centeredTabBar->isChecked());
	s.setValue("highLightTab", dialog_->highLightTab->isChecked());
	s.setValue("statusBarFrame", dialog_->statusBarFrame->isChecked());
	s.setValue("menuItemSpacing", dialog_->menuItemSpacing->value());
	s.setValue("buttonMenuItem", dialog_->buttonMenuItem->isChecked());
	s.setValue("menuBarEmphasis", dialog_->menuBarEmphasis->isChecked());
        s.setValue("colorizeSortedHeader", dialog_->colorizeSortedHeader->isChecked());
	s.setValue("menuBarEmphasisBorder", dialog_->menuBarEmphasisBorder->isChecked());
	s.setValue("customMenuBarEmphasisColor", dialog_->customMenuBarEmphasisColor->isChecked());
	s.setValue("menuBarEmphasisColor", QColor(dialog_->menuBarEmphasisColor->color()).name());

	s.setValue("menuStripe", dialog_->menuStripe->isChecked());
	s.setValue("shadowedButtonsText", dialog_->shadowedButtonsText->isChecked());
        s.setValue("shadowedMenuBarText", dialog_->shadowedMenuBarText->isChecked());
        s.setValue("sunkenShadows", dialog_->sunkenShadows->isChecked());
	s.setValue("animateProgressBar", dialog_->animateProgressBar->isChecked());
	s.setValue("lightBorder", dialog_->lightBorder->isChecked());
	s.setValue("animateButton", dialog_->animateButton->isChecked());
	s.setValue("animateButtonToDark", dialog_->animateButtonToDark->isChecked());
	s.setValue("drawToolBarSeparator", dialog_->drawToolBarSeparator->isChecked());
	s.setValue("drawToolBarItemSeparator", dialog_->drawToolBarItemSeparator->isChecked());
	s.setValue("drawTriangularExpander", dialog_->drawTriangularExpander->isChecked());
	s.setValue("inputFocusHighlight", dialog_->inputFocusHighlight->isChecked());
	s.setValue("buttonStyle", buttonType(dialog_->buttonStyle->currentIndex()));
	s.setValue("customOverHighlightColor", dialog_->customOverHighlightColor->isChecked());
	s.setValue("overHighlightColor", QColor(dialog_->overHighlightColor->color()).name());
	s.setValue("customFocusHighlightColor", dialog_->customFocusHighlightColor->isChecked());
	s.setValue("focusHighlightColor", QColor(dialog_->focusHighlightColor->color()).name());
	s.setValue("customCheckMarkColor", dialog_->customCheckMarkColor->isChecked());
	s.setValue("checkMarkColor", QColor(dialog_->checkMarkColor->color()).name());
}

void PolyesterStyleConfig::defaults()
{
	dialog_->useLowerCaseText->setChecked(false);
	dialog_->scrollBarLines->setChecked(false);
	dialog_->scrollBarStyle->setCurrentIndex(0);
	dialog_->scrollBarSize->setValue(16);
	dialog_->coloredScrollBar->setChecked(true);
	dialog_->centeredTabBar->setChecked(false);
	dialog_->highLightTab->setChecked(true);
	dialog_->statusBarFrame->setChecked(true);
	dialog_->menuBarEmphasis->setChecked(false);
	dialog_->menuBarEmphasisBorder->setChecked(true);
	dialog_->customMenuBarEmphasisColor->setChecked(false);
	dialog_->menuBarEmphasisColor->setColor("black");

	dialog_->menuStripe->setChecked(true);
	dialog_->animateProgressBar->setChecked(false);
        dialog_->colorizeSortedHeader->setChecked(true);
	dialog_->lightBorder->setChecked(true);
	dialog_->animateButton->setChecked(false);
	dialog_->animateButtonToDark->setChecked(false);
	dialog_->drawToolBarSeparator->setChecked(true);
	dialog_->drawToolBarItemSeparator->setChecked(true);
	dialog_->drawTriangularExpander->setChecked(false);
	dialog_->inputFocusHighlight->setChecked(true);
	dialog_->buttonStyle->setCurrentIndex(0);
	dialog_->customOverHighlightColor->setChecked(false);
	dialog_->overHighlightColor->setColor("black");
	dialog_->customFocusHighlightColor->setChecked(false);
	dialog_->focusHighlightColor->setColor("black");
	dialog_->customCheckMarkColor->setChecked(false);
	dialog_->checkMarkColor->setColor("black");
	//updateChanged would be done by setChecked already
}

void PolyesterStyleConfig::updateChanged()
{

	if ((dialog_->scrollBarLines->isChecked() == origScrollBarLines) &&
	    (dialog_->scrollBarStyle->currentText() == origScrollBarStyle) &&
	    (dialog_->scrollBarSize->value() == origScrollBarSize) &&
	    (dialog_->coloredScrollBar->isChecked() == origColoredScrollBar) &&
	    (dialog_->centeredTabBar->isChecked() == origCenteredTabBar) &&
	    (dialog_->highLightTab->isChecked() == origHighLightTab) &&
	    (dialog_->statusBarFrame->isChecked() == origStatusBarFrame) &&
	    (dialog_->menuItemSpacing->value() == origMenuItemSpacing) &&
	    (dialog_->buttonMenuItem->isChecked() == origButtonMenuItem) &&
	    (dialog_->menuBarEmphasis->isChecked() == origMenuBarEmphasis) &&
	    (dialog_->menuBarEmphasisBorder->isChecked() == origMenuBarEmphasisBorder) &&
	     (dialog_->customMenuBarEmphasisColor->isChecked() == origCustomMenuBarEmphasisColor) &&
	      (dialog_->menuBarEmphasisColor->color() == origMenuBarEmphasisColor) &&
	    (dialog_->menuStripe->isChecked() == origMenuStripe) &&
	    (dialog_->shadowedButtonsText->isChecked() == origShadowedButtonsText) &&
            (dialog_->shadowedMenuBarText->isChecked() == origShadowedMenuBarText) &&
            (dialog_->sunkenShadows->isChecked() == origSunkenShadows) &&
	    (dialog_->animateProgressBar->isChecked() == origAnimProgressBar) &&
	    (dialog_->lightBorder->isChecked() == origLightBorder) &&
            (dialog_->colorizeSortedHeader->isChecked() == origColorizeSortedHeader) &&
	    (dialog_->animateButton->isChecked() == origAnimButton) &&
	     (dialog_->animateButtonToDark->isChecked() == origAnimButtonToDark) &&
	    (dialog_->drawToolBarSeparator->isChecked() == origDrawToolBarSeparator) &&
	    (dialog_->drawToolBarItemSeparator->isChecked() == origDrawToolBarItemSeparator) &&
	    (dialog_->drawTriangularExpander->isChecked() == origDrawTriangularExpander) &&
	    (dialog_->inputFocusHighlight->isChecked() == origInputFocusHighlight) &&
	    (dialog_->customOverHighlightColor->isChecked() == origCustomOverHighlightColor) &&
	     (dialog_->overHighlightColor->color() == origOverHighlightColor) &&
	    (dialog_->buttonStyle->currentText() == origButtonStyle) &&
	    (dialog_->customFocusHighlightColor->isChecked() == origCustomFocusHighlightColor) &&
	     (dialog_->focusHighlightColor->color() == origFocusHighlightColor) &&
	    (dialog_->customCheckMarkColor->isChecked() == origCustomCheckMarkColor) &&
	     (dialog_->checkMarkColor->color() == origCheckMarkColor) &&
		(dialog_->useLowerCaseText->isChecked() == origUseLowerCaseText) 
	   )
		emit changed(false);
	else
		emit changed(true);
}

//button types
QString PolyesterStyleConfig::buttonType( int listnr )
{
    switch ( listnr ) {
        case 1:
            return QString("gradients");
	case 2:
	    return QString("reverseGradients");
	case 3:
	    return QString("flat");
        default:
            return QString("glass");
    }
}

int PolyesterStyleConfig::buttonItem( QString kBType )
{
    if( !kBType.compare("gradients") )
        return 1;
    else if( !kBType.compare("reverseGradients") )
        return 2;
    else if( !kBType.compare("flat") )
        return 3;
    else
        return 0;
}

//scrollBar types
QString PolyesterStyleConfig::scrollBarType( int listnr )
{
    switch ( listnr ) {
	case 1:
	    return QString("PlatinumStyleScrollBar");
	case 2:
	    return QString("NextStyleScrollBar");
        case 3:
            return QString("WindowsStyleScrollBar");
        case 4:
            return QString("NoButtonsScrollBar");
        default:
            return QString("ThreeButtonScrollBar");
    }
}

int PolyesterStyleConfig::scrollBarItem( QString kSType )
{
    if( !kSType.compare("PlatinumStyleScrollBar") )
        return 1;
    else if( !kSType.compare("NextStyleScrollBar") )
        return 2;
    else if( !kSType.compare("WindowsStyleScrollBar") )
        return 3;
    else if( !kSType.compare("NoButtonsScrollBar") )
        return 4;
    else
        return 0;
}

#include "moc_polyesterconf.cpp"
